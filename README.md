# README #

The RoboPell is a project to design and build a robotic pell stand for aid in Historical European Matial Arts (HEMA) training. This repsoitory contains the information necessary to build this project yourself including:

*Parts Lists
*Schematics
*Code

### How do I get set up? ###

First you must aquire all the parts needed and assemble them to have a physical pell stand.  This stand is based off of the stand designed by William Bushur at CKDF in Washington DC.  Starting by building this pell is reccommended.  After this, assembly of the robot base and uploading of the code is needed.  See the wiki for more information on how to do this and how to run the program once started.

### Contribution guidelines ###

Contributions are welcome, please contact Kyle Crandall with suggested modifications/improvements.  Feel free to submit pull requests as well.  With a pull request, you should include a description of what modifications you have made, and should also include any relevent changes to the wiki that would be required.

### Who do I talk to? ###

Kyle Crandall: crandalk@gmail.com
